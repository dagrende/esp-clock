#include <Arduino.h>
#include <SPI.h>

int latchPin = 15; // pin D8 on NodeMCU boards
//int clockPin = 14; // pin D5 on NodeMCU boards
//int dataPin = 13; // pin D7 on NodeMCU
byte numToSegs2[] {
  0b00111111, 0b00110000, 0b01011011, 0b01111001, 0b01110100,
  0b01101101, 0b01101111, 0b00111000, 0b01111111, 0b01111101
};
// byte numToSegs2[] {
//   0b00111111, 0b00000110, 0b01011011, 0b01111001, 0b01110100,
//   0b01101101, 0b01101111, 0b00111000, 0b01111111, 0b01111101
// };
byte numToSegs[] {
  0b00111111, 0b00000110, 0b01011011, 0b01001111, 0b01100110,
  0b01101101, 0b01111101, 0b00000111, 0b01111111, 0b01101111
};
byte digitPins[] = {D1, D2, D3, D4};
long num;

void setup() {
  //set pins to output so you can control the shift register
  pinMode(latchPin, OUTPUT);
  //pinMode(clockPin, OUTPUT);
  //pinMode(dataPin, OUTPUT);
  SPI.begin();

  for (unsigned int i = 0; i < sizeof digitPins; i++) {
    pinMode(digitPins[i], OUTPUT);
    digitalWrite(digitPins[i], 1);
  }

  num = 0;
}

void spiSend(byte sendByte) {
  digitalWrite(latchPin, LOW);
  SPI.transfer(sendByte);
  digitalWrite(latchPin, HIGH);
}

void loop() {
  int n = num;
  for (uint16 digit = 0; digit < sizeof digitPins; digit++) {
    digitalWrite(digitPins[digit], 0);
    byte *numToSegsx = digit == 2 ? numToSegs2 : numToSegs;
    byte sendByte = ~numToSegsx[n % 10];
    spiSend(sendByte);
    delay(1);
    digitalWrite(digitPins[digit], 1);
    n /= 10;
  }
  num = (num + 1) % 10000;
}
// byte b = 0;
// void loop() {
//   digitalWrite(digitPins[2], 0);
//   spiSend(~numToSegs2[b]);
//   delay(1000);
//   b = (b + 1) % 10;
// }
// byte b = 1;
// void loop() {
//   digitalWrite(digitPins[0], 0);
//   digitalWrite(digitPins[2], 0);
//   spiSend(~b);
//   delay(1000);
//   b <<= 1;
//   if (b == 0) {
//     b = 1;
//     delay(2000);
//   }
// }
